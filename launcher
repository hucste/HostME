#!/bin/bash
#set -x

###
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
#
# License: BSD Simplified
#
# Github: https://git.framasoft.org/hucste/HostME
#
# Date: 2016/04/17
#
###

EXEC=true
if ! ${EXEC}; then exit 1; fi

###
# Launcher Menu HostME
##

clear

pwd="$(pwd)"

# variables needed
. "${pwd}/config/vars"

#colors
. "${pwd}/config/colors"

# variables for displaying messages
. "${pwd}/config/mssg"
. "${pwd}/scripts/display"
. "${pwd}/scripts/system"

verify_need_softs



##########
### Functions
##########



function launch_menu() {

    detect_uid
    . "${pwd}/scripts/mng_menu"
    main_menu

    }

function launcher() {

    detect_pwd

    #if [[ ${NB_PARAMS} -eq 0 ]]; then

        #launch_menu
        #. "${pwd}/scripts/create_vhosts"
        #. "${pwd}/scripts/mng_ssl"
        #. "${pwd}/scripts/mng_ssh"
        #. "${pwd}/scripts/mng_headers"
        #create_vhost

    #else

        case "${MENU_CHOICE}" in
            create)
                launch_menu
                . "${pwd}/scripts/create_vhosts"
                . "${pwd}/scripts/mng_ssl"
                . "${pwd}/scripts/mng_ssh"
                . "${pwd}/scripts/mng_headers"
                create_vhost
            ;;
            delete)
                . "${pwd}/scripts/delete_vhosts"
                launch_menu
                delete_domain
            ;;
            help)
                display_help
            ;;
            restart)
                case "${MENU_OPTION}" in
                    "php"|"web")
                        detect_uid
                        restart_server "${MENU_OPTION}"
                    ;;
                    *)
                        display_bad_launcher
                        display_help
                    ;;
                esac
            ;;
            ssl)
                detect_uid
                . "${pwd}/scripts/mng_menu"
                . "${pwd}/scripts/mng_ssl"
                case "${MENU_OPTION}" in
                    "create"|"new")
                        main_menu
                        use_ssl
                    ;;
                    "cron")
                        # menu "${MENU_ARG}"
                        main_menu
                        ssl_cron
                    ;;
                    "final")
                        main_menu
                        finalize_ssl
                    ;;
                    "renew")
                        main_menu
                        ssl_renew
                    ;;
                    *)
                        display_bad_launcher
                        display_help
                    ;;
                esac
            ;;
            test)
                # ajouter test zone
                case "${MENU_OPTION}" in
                    "cfg")
                        case "${MENU_ARG}" in
                            "php"|"web")
                                test_cfg "${MENU_ARG}"
                            ;;
                            *)
                                display_bad_launcher
                                display_help
                            ;;
                        esac
                    ;;
                    "child")
                        launch_menu
                        optimize_max_childs
                    ;;
                    "domain")
                        . "${pwd}/scripts/mng_menu"
                        main_menu
                        test_conn_domain "${PARAMS[3]}"
                    ;;
                    "mem")
                        case "${MENU_ARG}" in
                            "php"|"web")
                                check_memory_usage "${MENU_ARG}"
                            ;;
                            *)
                                display_bad_launcher
                                display_help
                            ;;
                        esac
                    ;;
                    "ocsp")
                        . "${pwd}/scripts/mng_menu"
                        main_menu
                        . "${pwd}/scripts/mng_ssl"
                        test_ocsp_stapling
                    ;;
                    *)
                        display_bad_launcher
                        display_help
                    ;;
                esac
            ;;
            view)
                . "${pwd}/scripts/mng_menu"
                case "${MENU_OPTION}" in
                    "cfg")
                        case "${MENU_ARG}" in
                            "php"|"web")
                                main_menu
                                display_cfg "${MENU_ARG}"
                            ;;
                            *)
                                display_bad_launcher
                                display_help
                            ;;
                        esac
                    ;;
                    "log")
                        case "${MENU_ARG}" in
                            "php"|"web")
                                main_menu
                                view_log "${MENU_ARG}"
                            ;;
                            *)
                                display_bad_launcher
                                display_help
                            ;;
                        esac
                    ;;
                    *)
                        display_bad_launcher
                        display_help
                    ;;
                esac
            ;;
            *)
                display_bad_launcher
            ;;
        esac

    #fi

    }

launcher
