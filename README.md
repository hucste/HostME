HostME
======

Author
------
**@author**: Stéphane HUC <br />
**@email**: devs@stephane-huc.net <br />
**@gpg:id**: 0x6135D4404D44BD58 <br />
**@gpg:fingerprint**: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58 <br />
**@license**: BSD-Simplified

---

But :
-----

**Gérer la création des fichiers nécessaires aux  services web+php ... et de
 les configurer** selon les réponses données.

**Ce que ne fait pas ce projet :** <br />
Il n'installe pas les packages nécessaires aux services web - *de forte préférence
nginx* - php, openssl, voire ssh ! <br />
 **Faites-le vous même.**


Principes :
----------

Partant du principe que $domain est le domaine déclaré, de type 'example.com' :

- Le fichier nginx principal sera dans /etc/nginx/sites-availables/$domain <br />
- Les autres fichiers de configuration liés sont dans /etc/nginx/cfg/$domain
- Le fichier php5-fpm principal sera /etc/php5/fpm/pool/$domain.conf

Le script vérifiera au moment de la création que l'écriture du domaine est
 correcte.<br />
 **Il vérifie une correspondance entre le domaine désiré et l'existence probable
  d'un domaine déjà géré par ce script ...<br />
  mais ne vérifie pas que le domaine soit acheté !**

  _Si une correspondance est trouvée, le script s'arrête, et vous demande d'analyser
   le pourquoi !_

Création utilisateurs :
-----------------------

Normalement, un utilisateur système est créé par $domain créé, sans droit
 particulier. Celui-ci est ajouté par défaut au groupe lié au serveur nginx. <br />
 Cet utilisateur a les droits sur les répertoires 'cache', 'www', etc ...
 dans le répertoire racine du $domain : /srv/www/$domain

_Si SSH est installé et utilisé, il vous demandera l'ajout à un groupe SSH, que
 vous aurez préalablement créé, géré ; voir la section SSH, plus bas !_

Configuration :
---------------

Il est possible de changer certaines valeurs de configuration, telle que la
 racine web, qui par défaut est '/srv/www/', _(variable **home**)_<br />
 - voir le fichier /config/vars

D'ailleurs, je vous recommande de changer la variable **email** - c'est votre
 email de contact.

**ATTENTION** : Si vous changez des valeurs, faites-le absolument avant le
 lancement du script. **Toutes modifications, que vous faites, sont à vos risques
 et périls - je ne suis en rien responsable de vos propres erreurs !<br />
 Assurez-vous par plusieurs relectures de vos propres modifications, avant de
 lancer le script**

**NE SUPPRIMEZ JAMAIS la déclaration faite en ligne 4, dans chacun des scripts :<br />
 <code>if ! ${EXEC}; then exit; fi</code> <br />
 **N'ÉCRIVEZ JAMAIS aucun code avant cette ligne !**


Utilisation :
-------------

Téléchargez-le sur : [https://git.framasoft.org/hucste/HostME/tree/master][1]

Donnez des **droits 0700** au script bash principal, nommé **launcher** <br />
Exécutez-le avec les **droits administrateur** !<br />
_Si vous ne le faites pas, il s'arrêtera et vous le rappellera._

Puis, répondez aux questions posées.

_Si vous l'executez avec l'option 'help', il vous sera affiché le fichier d'aide !<br />
Je vous conseille de la tester, vous aurez ainsi un aperçu des différentes options possibles ..._

---

HTTP
----

**nginx**

Le script cherche à détecter le group et l'utilisateur lié au serveur nginx.
Et l'utilisera pour modifier la configuration php5-fpm ...

Par défaut, l'utilisateur et le group nginx sont 'www-data', réciproquement.

**Headers _(entêtes HTTP)_**

Toutes les déclarations **add_header** sont écrites **dans le fichier**
 /samples/**headers.cfg** <br />
 Ce fichier sera _copié : dans /etc/nginx/cfg/$domain/ - **modifier celui-ci
 si besoin**_

_Par défaut, elles sont assez restrictives, pour les désactiver momentanément,
 commentez la ligne include relative dans le fichier de configuration lié au
 domaine : /etc/nginx/sites-availables/$domain_

**Rotation des journaux _(log rotation)_**

Le script est capable d'ajouter la gestion de rotation des journaux. Il créé un
 fichier /etc/logrotate.d/manage_domain_logs

L'utilisateur, par défaut, des fichiers logs est 'adm'.

_**Attention** : si vous avez modifié la racine des répertoires web,
 (la variable **home**), le script modifie le fichier en question_

---

HTTPS
-----

Ce script vous permet(tra ?) de **configurer correctement le protocole HTTPS**.

Les fichiers de configuration liés à l'usage de SSL, sont :

- /etc/nginx/cfg/$domain/port_https.cfg
- /etc/nginx/cfg/$domain/ssl.cfg
- cela impacte aussi le fichier /etc/nginx/cfg/$domain/headers.cfg

**Recommandations :**

Dans un premier temps, créez un domaine, et vérifier qu'il fonctionne correctement
 sur le protocole HTTP. <br />
Une fois assuré de ce fait, utilisez le menu pour créer les clés SSL nécessaires
 et/ou utilisez [Let's Encrypt][5]. Le script peut télécharger le client officiel,
 à partir du [dépôt GIT][6], et l'utilise en mode 'webroot', sans arrêter le service
 HTTP.

 _Préférez l'usage du projet LE ; il est prévu, dans le script, de se "greffer"
 sur d'autres certificateurs d'autorité, tels que Gandi, StartSSL, voire Cacert ...
 mais le code bien que créé n'a pas été testé !_

**Ce que ne fait pas non plus ce projet :**<br />
_Pour l'instant, "what's else!"_

---

PHP
---

L'utilisateur système lié au $domain est invoqué par le service php5-fpm.

**Certaines fonctions PHP sont désactivées** par défaut - cf, le **fichier**
 /samples/**pool.example**: <br />
 php_admin_value[disable_functions]

**De même, par défaut, sont activées certaines sécurités, telles que :** <br />
php_admin_value[open_basedir] <br />
php_value[max_execution_time] <br />
_Il est très possible que de telles options vous empêchent d'installer correctement,
 c'est-à-dire sans soucis, certains scripts ; si c'est le cas, pensez à les désactiver
 en les commentant par ';', puis relancez le service php-fpm, voire web ..._

 _Ce fichier deviendra /etc/php5/fpm/pool.d/$domain.conf - modifier celui-ci
 si besoin !_

---

SSH
---

**Recommandations :**

**Ajouter vous-même un group ssh**, lié à la gestion des domaines, sans aucun
droit particulier ... **puis modifier votre fichier /etc/ssh/sshd_config**,
ainsi :

<code>
AllowGroups sshdom
Match group sshdom
&nbsp;&nbsp;&nbsp;&nbsp;ForceCommand internal-sftp
&nbsp;&nbsp;&nbsp;&nbsp;ChrootDirectory /home/%u
</code>

**Concept**

Le script vous demandera lors de la création de domaine si vous voulez utiliser
 SSH, et si oui, il ajoutera l'utilisateur système lié au $domain dans le groupe
 SSH que vous voudrez. De l'intérêt de créer un groupe SSH qui sera lui cloisonner,
 tel qu'expliqué ci-dessus.

**Le script n'installe pas SSH, ni ne le configurera pour vous ... à vous de le faire !**

**ATTENTION** : Pensez à **modifier la variable email dans le fichier
 /config/vars** de ce projet ; mettez votre email de contact, auprès des CA.

Si le projet [MySecureShell][3] est installé et actif, le script va tenter de modifier
 la configuration lié à l'utilisateur système du $domain pour que son shell soit
 géré par MSS. <br />
 **Ce script n'installe pas et ne configure pas MSS.  C'est à vous de le faire,
 avant, si besoin.**

Si l'outil [SSLH][4] est installé, avant, et détecté lors de la création du $domain,
 le script modifiera un des fichiers de config nginx lié au $domain, pour que la
 redirection du port 443 soit à l'écoute de l'interface réseau correspondante,
 et du port modifié correspondant !

 **Ce script n'installe pas et ne configure pas SSLH.
 C'est à vous de le faire, avant, si besoin** - <br />
 _Si vous avez paramétré SSLH, et/ou modifié après l'exécution de ce script, vous
 devrez modifier à la main le fichier /etc/nginx/cfg/$domain/port_https.cfg
 correspondant à un domaine particulier !_

---

Enjoy-it!
---------

!!! It's still into development !!! <br />
!!! Use at yours personals risks !!!

---

Forum
-----

Si besoin, venez en discuter sur [Debian-fr.org][2]



[1]: https://git.framasoft.org/hucste/HostME/tree/master
[2]: https://www.debian-fr.org/t/projet-hostme/68871/1
[3]: http://mysecureshell.sourceforge.net/fr/index.html
[4]: http://www.rutschle.net/tech/sslh.shtml
[5]: https://letsencrypt.org/
[6]: https://github.com/letsencrypt/letsencrypt
